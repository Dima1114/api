#!/bin/sh

# any future command that fails will exit the script
set -e
# Lets write the public key of our aws instance
eval $(ssh-agent -s)
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null

# ** Alternative approach
# echo -e "$PRIVATE_KEY" > /root/.ssh/id_rsa
# chmod 600 /root/.ssh/id_rsa
# ** End of alternative approach

# disable the host key checking.
chmod +x ./deploy/disableHostKeyChecking.sh
./deploy/disableHostKeyChecking.sh

# we have already setup the DEPLOYER_SERVERS in our gitlab settings which is a
# comma seperated values of ip addresses.
# lets split this string and convert this into array
# In UNIX, we can use this commond to do this
# ${string//substring/replacement}
# our substring is "," and we replace it with nothing.
server=$DEPLOY_SERVERS
#array=(${string//,/ })

# Lets iterate over this array and ssh into each EC2 instance
# Once inside.
# 1. copy jar to a server
# 2. Start the server
#for server in "${array[@]}"
#do
# Read certificate stored in $PRIVATE_KEY variable and save it in a new file
echo "$PRIVATE_KEY" > "$(pwd)/private.pem"
key_file="$(pwd)/private.pem"

echo "deploying to ${server}"
jar_path=$(find build/libs/ -type f -name "*.jar")
jar_name=$(basename ${jar_path})
echo ${jar_name}
ssh ubuntu@${server} "killall java && find . -type f -iname \*.jar -delete"
echo "uploading ${jar_name} to ${server}"
scp -i ${key_file} ${jar_path} ubuntu@${server}:~
ssh ubuntu@${server} "nohup java -jar ${jar_name} & exit"
#done # pkill -f 'java -jar' &&