package com.k8s.app

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.CrossOrigin
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@SpringBootApplication
@CrossOrigin
@RestController
class AppApplication {

    @GetMapping("/")
    fun get(): String = "Hello, k8s!"
}

fun main(args: Array<String>) {
    runApplication<AppApplication>(*args)
}
