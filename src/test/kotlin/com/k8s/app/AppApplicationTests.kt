package com.k8s.app

import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.junit.MockitoJUnitRunner
import kotlin.test.assertEquals

@RunWith(MockitoJUnitRunner::class)
class AppApplicationTests {

	private val testSubject = AppApplication()

	@Test
	fun contextLoads() {}

	@Test
	fun testGetHello(){

		//when
		val result = testSubject.get()

		//then
		assertEquals("Hello, k8s!", result)
	}

}
